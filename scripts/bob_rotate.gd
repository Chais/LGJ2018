extends Node2D

export (float) var amplitude = 8
export (float) var frequency = 1
export (float) var offset = -32
export (float) var degreesPerSecond = 30

var time = 0

func _process(delta):
	position.y = offset + sin(time * frequency) * amplitude
	set_rotation_degrees(get_rotation_degrees() + degreesPerSecond * delta)
	time = wrapf(time + delta, 0, 2 * PI)
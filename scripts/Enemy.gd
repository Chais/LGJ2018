extends KinematicBody2D

export(float) var gravity = 34.3
export(float) var fallFactor = 4
export(float) var walkSpeed = 200
export(float) var runSpeed = 600
export(float) var jumpSpeed = 940
export(float) var viewingDistance = 600

var player
var last_player_pos = null
var velocity = Vector2()
var friction = 0

func _ready():
	player = get_parent().get_node("Player")

func _physics_process(delta):
	if velocity.y > 0:
		velocity.y += gravity * fallFactor
	else:
		velocity.y += gravity

	if can_see_player():
		get_node("Polygon2D").color = Color("00ff00")
		last_player_pos = player.global_position
	else:
		get_node("Polygon2D").color = Color("ff0000")
	if last_player_pos:
		walk_towards_player()
	velocity = move_and_slide(velocity, Vector2(0, -1))

func can_see_player():
	var player_pos = player.global_position
	if global_position.distance_to(player_pos) <= viewingDistance:
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, player_pos, [self], 1025)
		return result.collider == player
	else:
		return false

func walk_towards_player():
	var friction = 0.8
	var x_distance = (last_player_pos - global_position).x
	if abs(x_distance) < 5:
		x_distance = 0
	var walk = sign(x_distance) * walkSpeed
	var slides = get_slide_count()
	var collision = null
	if slides > 0:
		collision = get_slide_collision(slides - 1)
	velocity.x = velocity.x * (1 - friction) + walk
	if is_on_floor() && last_player_pos.y < global_position.y - 63:
		velocity.y -= jumpSpeed
